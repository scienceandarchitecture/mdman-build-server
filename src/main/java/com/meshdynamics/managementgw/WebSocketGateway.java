package com.meshdynamics.managementgw;

import java.io.IOException;
import java.nio.channels.DatagramChannel;
import java.util.Map;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.log4j.Logger;

import com.meshdynamics.exceptions.MDBuildException;
import com.meshdynamics.helper.ErrorCodes;
import com.meshdynamics.helper.Helper;
import com.meshdynamics.helper.WebSocketMappingCache;
import com.meshdynamics.security.SecurityGateWay;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */
@ServerEndpoint("/build")
public class WebSocketGateway {

	/**
	 * Field LOGGER.
	 */
	static final Logger LOGGER = Logger.getLogger(WebSocketGateway.class);
	/**
	 * Field channel.
	 */
	static DatagramChannel channel;

	/**
	 * Method onOpen.
	 * 
	 * @param session
	 *            Session
	 */
	@OnOpen
	public void onOpen(Session session) {
		LOGGER.info("Client Connected with Session - : " + session);
		
	}

	static {
		try {
			channel = DatagramChannel.open();
		} catch (IOException e) {
			Helper.printStackTrace(e);
			throw new MDBuildException(ErrorCodes.WEB_SOCKET_ERROR.getCode(), ErrorCodes.WEB_SOCKET_ERROR.getValue(), e);
			
		}
	}

	/**
	 * Method echoTextMessage.
	 * 
	 * @param session
	 *            Session
	 * @param msg
	 *            String
	 * @param last
	 *            boolean
	 * @throws IOException 
	 */
	@OnMessage
	public void onMessage(Session session, String msg, boolean last) throws IOException {
		LOGGER.trace("Plain Text Message  Received -: " + msg);
		Map<String, Session> requestedBuildCache = WebSocketMappingCache.getRequestedBuildCache();
		if (null != requestedBuildCache) {
			requestedBuildCache.put(msg, session);
			session.getBasicRemote().sendText("key:"+SecurityGateWay.bytesToHex(msg.getBytes()));
		}
		
		
	}

	/**
	 * Method echoBinaryMessage.
	 * 
	 * @param session
	 *            Session
	 * @param bb
	 *            byte[]
	 * @param last
	 *            boolean
	 */
	@OnMessage
	public void echoBinaryMessage(Session session, byte bb[], boolean last) {

	}

	/**
	 * Method Onclose.
	 * 
	 * @param session
	 *            Session
	 */
	@OnClose
	public void Onclose(Session session) {
       try {
		session.close();
	} catch (IOException e) {
		Helper.printStackTrace(e);
		throw new MDBuildException(ErrorCodes.WEB_SOCKET_ERROR.getCode(), ErrorCodes.WEB_SOCKET_ERROR.getValue(), e);
	}
	}
	
	 /**
	  * Method onnError
	 * @param session
	 * @param throwable
	 */
	@OnError
    public void onError( Session session, Throwable throwable)
    {
		
        if (!(session != null? session.isOpen() : false)) {
            LOGGER.warn( "Received application-unhandled throwable (socket already closed): ", throwable);
            return;
        }
       
        	LOGGER.warn("Received unknown execption: ", throwable);
        	throwable.printStackTrace();
    }
	 
	 

}