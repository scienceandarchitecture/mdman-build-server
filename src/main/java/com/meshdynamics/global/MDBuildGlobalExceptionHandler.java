package com.meshdynamics.global;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.meshdynamics.exceptions.MDBuildException;
import com.meshdynamics.exceptions.RequestPreConditionsFailedException;
import com.meshdynamics.helper.BuildError;
import com.meshdynamics.helper.ErrorCodes;

@ControllerAdvice
public class MDBuildGlobalExceptionHandler{
	    @ExceptionHandler({ RequestPreConditionsFailedException.class })
	    protected ResponseEntity<Object> handleInvalidRequest(RuntimeException e, WebRequest request) {
		 
		    RequestPreConditionsFailedException ire = (RequestPreConditionsFailedException) e;
	        List<BuildError> fieldErrorResources = new ArrayList<>();

	        List<FieldError> fieldErrors = ire.getErrors().getFieldErrors();
	        for (FieldError fieldError : fieldErrors) {
	        	BuildError fieldErrorResource = new BuildError();
	            fieldErrorResource.setCode(ErrorCodes.VALIDATION_FAILED.getCode());
	            fieldErrorResource.setMessage(ErrorCodes.VALIDATION_FAILED.getValue());
	            fieldErrorResource.setError(fieldError.getField()+""+fieldError.getDefaultMessage());
	            fieldErrorResources.add(fieldErrorResource);
	        }

	        return new ResponseEntity<Object>(fieldErrorResources, HttpStatus.OK);
	    }
	    
	    @ExceptionHandler({ Exception.class })
	    protected ResponseEntity<Object> handleException(Exception e, WebRequest request) {
		 
		    
	        List<BuildError> errorResources = new ArrayList<>();

	        BuildError error = new BuildError();
	        error.setCode(ErrorCodes.BUILD_SERVER_ERROR.getCode());
	        error.setMessage(ErrorCodes.BUILD_SERVER_ERROR.getValue());
	        error.setError(e.getMessage());
	        errorResources.add(error);

	        return new ResponseEntity<Object>(errorResources, HttpStatus.OK);
	    }
	    
	    @ExceptionHandler({ MDBuildException.class })
	    protected ResponseEntity<Object> handleMDBuildException(RuntimeException e, WebRequest request) {
		 
		    MDBuildException mde = (MDBuildException)e;
	        List<BuildError> errorResources = new ArrayList<>();

	        BuildError error = new BuildError();
	        error.setCode(mde.getCode());
	        error.setMessage(mde.getMessage());
	        error.setError(mde.getMessage());
	        errorResources.add(error);

	        return new ResponseEntity<Object>(errorResources, HttpStatus.OK);
	    }
	
	

}
