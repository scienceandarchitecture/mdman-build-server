package com.meshdynamics.global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
public class MDBuildRequestInterceptor implements HandlerInterceptor {
	
	
	static final Logger LOGGER = Logger.getLogger(MDBuildRequestInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)throws Exception {
		
		LOGGER.trace("MDBuildRequestInterceptor.preHandle() started");
		//Update Request
		
		
		LOGGER.trace("MDBuildRequestInterceptor.preHandle() ends");
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,ModelAndView modelAndView) throws Exception {
		LOGGER.trace("MDBuildRequestInterceptor.postHandle() started");
		
		
		
		LOGGER.trace("MDBuildRequestInterceptor.postHandle() ends");
		
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)throws Exception {
		
        LOGGER.trace("MDBuildRequestInterceptor.afterCompletion() started");
		
		
		
		LOGGER.trace("MDBuildRequestInterceptor.afterCompletion() ends");
		
	}

}
