package com.meshdynamics.global;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.meshdynamics.common.Constant;
import com.meshdynamics.helper.MDProperties;


@Scope(value="singleton")
public class GlobalDirectoryLookup {
	
	
	
	static final Logger LOGGER = Logger.getLogger(GlobalDirectoryLookup.class);
	private static File targetDirectory;
	
	private GlobalDirectoryLookup() {
		super();
	
	}
	
	public static File getRootDirectory(){

		if(null == targetDirectory){

			final String configDir = MDProperties.get(Constant.BUILD_PATH);//Path where all images resides
			targetDirectory = new File(configDir);
			if(!targetDirectory.exists()){
				//throw exception as target directory is not found.
				LOGGER.error("Build path was not configured -"+targetDirectory);

			}
		}

		return targetDirectory;
	}
	
	
	public static File getBuildHistroyLogFile(){
        
	    return getRootDirectory().toPath().resolve(MDProperties.get(Constant.BUILD_HISTORY_LOG)).toFile();//return buildhistory.log file path
	
	}

	

}
