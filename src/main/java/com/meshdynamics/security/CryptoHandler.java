package com.meshdynamics.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.apache.log4j.Logger;

import com.meshdynamics.exceptions.MDBuildException;
import com.meshdynamics.helper.ErrorCodes;
import com.meshdynamics.helper.Helper;

public class CryptoHandler {
	
	static final Logger LOGGER = Logger.getLogger(CryptoHandler.class);
	private static final String KEY = "password";
	private CryptoHandler() {
		super();
		
	}
	
	private static String getSecurePassword(String authKey)
    {
        String digest = null;
        try {
        	 // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(authKey.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            digest = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
        	Helper.printStackTrace(e);
        	throw new MDBuildException(ErrorCodes.API_KEY_INVALID.getCode(), ErrorCodes.API_KEY_INVALID.getValue(), e);
        	
        }
        return digest;
    }
	
	

	public static boolean isAuthKeyMatched(String key)
	{
		
		boolean isMatched = false;
		String digest = getSecurePassword(KEY);
		if(digest != null && digest.equals(key))
		{
			isMatched = true;
		}
		return isMatched;
	}
	

	 /**
     * 1. Generate a plain text for encryption
     * 2. Get a secret key (printed in hexadecimal form). In actual use this must 
     * by encrypted and kept safe. The same key is required for decryption.
     * 3. 
     */
    public static void main(String[] args) throws Exception {
        String plainText = "Hello World";
        SecretKey secKey = getSecretEncryptionKey();
        byte[] cipherText = encryptText(plainText, secKey);
        String decryptedText = decryptText(cipherText, secKey);
        
        System.out.println("Original Text:" + plainText);
        System.out.println("Key in encrypt:"+new String(secKey.getEncoded()));
        /*System.out.println("AES Key (Hex Form):"+bytesToHex(secKey.getEncoded()));
        byte[] bytes = new BigInteger("7F" + bytesToHex(secKey.getEncoded()), 16).toByteArray();
        SecretKeySpec key = new SecretKeySpec(bytes, 1, bytes.length-1, "AES");
        System.out.println("Reverted Key:"+bytesToHex(key.getEncoded()));
        
        System.out.println("Encrypted Text (Hex Form):"+bytesToHex(cipherText));*/
        System.out.println("Descrypted Text:"+decryptedText);
        
    }
    
    /**
     * gets the AES encryption key. In your actual programs, this should be safely
     * stored.
     * @return
     * @throws Exception 
     */
    public static SecretKey getSecretEncryptionKey(){
    	SecretKey secKey = null;
    	try{
          KeyGenerator generator = KeyGenerator.getInstance("AES");
           generator.init(128); // The AES key size in number of bits
           secKey = generator.generateKey();
    	}catch(Exception e){
    		Helper.printStackTrace(e);
    		throw new MDBuildException(ErrorCodes.SECURITY_ERROR.getCode(), ErrorCodes.SECURITY_ERROR.getValue(), e);
    	}
    	return secKey;
        
    }
    
    /**
     * Encrypts plainText in AES using the secret key
     * @param plainText
     * @param secKey
     * @return
     * @throws Exception 
     */
    public static byte[] encryptText(String plainText,SecretKey secKey){
		byte[] cipherByte = new byte[1024];
		
		try{
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
        cipherByte= aesCipher.doFinal(plainText.getBytes());
         }catch(Exception e){
    		Helper.printStackTrace(e);
    		throw new MDBuildException(ErrorCodes.SECURITY_ERROR.getCode(), ErrorCodes.SECURITY_ERROR.getValue(), e);
    	}
         return cipherByte;
        
    }
    
    /**
     * Decrypts encrypted byte array using the key used for encryption.
     * @param byteCipherText
     * @param secKey
     * @return
     * @throws Exception 
     */
    public static String decryptText(byte[] byteCipherText, SecretKey secKey){
		String text = "";
		try{
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.DECRYPT_MODE, secKey);
        byte[] bytePlainText = aesCipher.doFinal(byteCipherText);
        text = new String(bytePlainText);
		}catch(Exception e){
    		Helper.printStackTrace(e);
    		throw new MDBuildException(ErrorCodes.SECURITY_ERROR.getCode(), ErrorCodes.SECURITY_ERROR.getValue(), e);
    	}
         return text;
    }
    
    

}
