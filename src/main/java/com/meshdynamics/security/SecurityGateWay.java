package com.meshdynamics.security;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

import com.meshdynamics.exceptions.MDBuildException;
import com.meshdynamics.helper.Helper;

/**
 */
public class SecurityGateWay {
	
	static final Logger LOGGER = Logger.getLogger(SecurityGateWay.class);
	private SecurityGateWay() {
		super();
		
	}
	
	//public static boolean isAuthorized(String password) {
	//	return CryptoHandler.isAuthKeyMatched(password);
	//}
	
	public static String getSecretKeyInHex(){
		final SecretKey key = getSecretKey();
		return bytesToHex(key.getEncoded());
	}
	

	/**
     * Convert a binary byte array into readable hex form
     * @param hash
     * @return 
     */
	public static String  bytesToHex(byte[] hash) {
        return DatatypeConverter.printHexBinary(hash);
    }
	
	public static String encryptData(String msgToEncrypt,String encodedKey) {
		
	    byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        // rebuild key using SecretKeySpec
        SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
		return encryptText(msgToEncrypt, originalKey);
		
	}
	
    public static String decryptData(String msgToDecrypt,String encodedKey) {
   
	    byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        // rebuild key using SecretKeySpec
        SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
        byte[] decodedString = Base64.getDecoder().decode(msgToDecrypt);
		return decryptText(decodedString, originalKey);
		
	}
    
    /**
     * gets the AES encryption key. In your actual programs, this should be safely
     * stored.
     * @return secKey {@link SecretKey}
     */
    public static SecretKey getSecretKey(){
    	SecretKey secKey = null;
    	try{
           KeyGenerator generator = KeyGenerator.getInstance("AES");
           generator.init(128); // The AES key size in number of bits
           secKey = generator.generateKey();
    	}catch(Exception e){
    		Helper.printStackTrace(e);
    		throw new MDBuildException("","",e);//ErrorCodes.SECURITY_ERROR.getCode(), Apicodes.SECURITY_ERROR.getValue(), e);
    	}
    	return secKey;
        
    }
    
    /**
     * Encrypts plainText in AES using the secret key
     * @param plainText
     * @param secKey
     */
    public static String encryptText(String plainText,SecretKey secKey){
		byte[] cipherByte = new byte[1024];
		
		try{
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
        cipherByte= aesCipher.doFinal(plainText.getBytes());
         }catch(Exception e){
    		Helper.printStackTrace(e);
    		throw new MDBuildException();//Apicodes.SECURITY_ERROR.getCode(), Apicodes.SECURITY_ERROR.getValue(), e);
    	}
         return new String(Base64.getEncoder().encode(cipherByte));
        
    }
    
    /**
     * Decrypt encrypted byte array using the key used for encryption.
     * @param byteCipherText
     * @param secKey
     * @return text String
     */
    public static String decryptText(byte[] byteCipherText, SecretKey secKey){
		String text = "";
		try{
        Cipher aesCipher = Cipher.getInstance("AES");
        aesCipher.init(Cipher.DECRYPT_MODE, secKey);
        byte[] bytePlainText = aesCipher.doFinal(byteCipherText);
        text = new String(bytePlainText);
		}catch(Exception e){
    		Helper.printStackTrace(e);
    		throw new MDBuildException();//Apicodes.SECURITY_ERROR.getCode(), Apicodes.SECURITY_ERROR.getValue(), e);
    	}
         return text;
    }
	

}
