package com.meshdynamics.socket.client;

import java.net.URI;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.apache.log4j.Logger;

@ClientEndpoint
public class WSCleint {
	
	static final Logger LOGGER = Logger.getLogger(WSCleint.class);

	private static Object waitLock = new Object();

	@OnMessage
	public void onMessage(String message) {
		System.out.println("Received msg: " + message);
	}

	

	public static void main(String[] args) {
		
		Session session = null;
		try {
			// Tyrus is plugged via ServiceLoader API. See notes above
			final WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			// MDBuild is the context-root of my web.app
			// build is the path given in the ServerEndPoint annotation on
			// server implementation
			LOGGER.info("Connecting to Web Socket server....");
			session = container.connectToServer(WSCleint.class, URI.create("ws://localhost:8080/MDBuild/build"));
			if(session.isOpen())
			{
				LOGGER.info("Connection Established! "+session.getId());
				
				//echo message
				session.getBasicRemote().sendText("Shekhar");
				
				
				
			}
			//wait4TerminateSignal();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {
					LOGGER.error(e.getMessage());
				}
			}
		}
	}

}
