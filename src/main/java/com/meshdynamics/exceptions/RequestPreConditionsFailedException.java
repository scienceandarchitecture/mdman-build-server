package com.meshdynamics.exceptions;

import org.springframework.validation.Errors;

@SuppressWarnings("serial")
public class RequestPreConditionsFailedException extends RuntimeException {
	private   Errors errors;

    public RequestPreConditionsFailedException(String message, Errors errors) {
        super(message);
        this.errors = errors;
    }

    public Errors getErrors() { return errors; }
}
