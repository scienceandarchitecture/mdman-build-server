package com.meshdynamics.exceptions;

public class MDBuildException extends RuntimeException{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3169519374326125484L;
	
	private String code;
	private String message;
	private Throwable exception;
	
	public MDBuildException() {
		super();
		
	}
	
	
	public MDBuildException(String code, String message,Throwable t) {
		super();
		this.code = code;
		this.message = message;
		this.exception = t;
	}


	
	public String getCode() {
		return code;
	}


	public String getMessage() {
		return message;
	}


	public Throwable getException() {
		return exception;
	}
	
	

}
