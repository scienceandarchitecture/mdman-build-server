package com.meshdynamics.build;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.meshdynamics.common.Constant;
import com.meshdynamics.helper.Helper;
import com.meshdynamics.helper.MDProperties;
import com.meshdynamics.helper.RunningBuildCache;
import com.meshdynamics.vo.RequestVo;

/**
 */
public class BuildProcessor extends Thread {

	static final Logger LOGGER = Logger.getLogger(BuildProcessor.class);
	private static BlockingQueue<RequestVo> queue;
	
	
    public BuildProcessor(){
    	queue = new LinkedBlockingQueue<>();
    }
	/**
	 * Method queueRequest.
	 * @param requestVo RequestVo
	 */
	public static void queueRequest(RequestVo requestVo) {
		try {
			
			queue.put(requestVo);
			LOGGER.trace("BuildRequest Received - "+ requestVo.getMacAddress());

		} catch (InterruptedException ie) {
			Helper.printStackTrace(ie);
			LOGGER.error(ie.getMessage());
		} catch (Exception e) {
			Helper.printStackTrace(e);
			LOGGER.error(e.getMessage());
		}
	}

	/**
	 * Method run.
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		LOGGER.trace("BuildProcessor started...");
       String result = null;
		try {
			while (true) {
				if (null != queue && !queue.isEmpty()){
					try {
						RequestVo	requestVo = queue.take();
						LOGGER.info("Build Request Dequed For "+ requestVo.getMacAddress());
						RunningBuildCache.getRunningBuildCache().put(requestVo.getMacAddress(), requestVo);
						// triggering the build command
						String buildName = requestVo.getBuildName();
						String command = requestVo.getCommand();//preparing the native command to execute in Linux OS
						
						
				        File dir = new File(MDProperties.get(Constant.BUILD_COMMAND_DIR)).toPath().resolve(requestVo.getVersion()).resolve(requestVo.getTargetType().replace("_md", "").trim()).toFile();
						//File dir= new File(MDProperties.get(Constant.BUILD_COMMAND_PATH+requestVo.getTargetType().toLowerCase()));
						if(LOGGER.isTraceEnabled()){
							LOGGER.trace("Build Command path "+dir.getAbsolutePath());
							LOGGER.trace("Build Command prepared "+command);
						}
						
						Process process= Runtime.getRuntime().exec(command,null,dir);
						process.waitFor();
						
						//log build history
						final boolean isBuildLogged = Helper.isBuildAdded(requestVo.getVersion()+"/"+buildName);
						if(isBuildLogged)
						{
							LOGGER.info("Build History for-"+buildName+"was added");
						}
						
						if(null != process )
						{	
							BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
				            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
	
				            // read the output from the command			            
				            while ((result = stdInput.readLine()) != null) {
				                LOGGER.info("Build command execution successful "+result);
				            }		            
				            // read any errors from the attempted command
				            while ((result = stdError.readLine()) != null) {
				            	LOGGER.info("Build command execution failure "+result);
				            }
					     
					    	  
					      }
							
						}
						//else{
						//	LOGGER.warn("Command executed without fail,But delivered empty result");
						//}
					 catch(Exception e) {
						LOGGER.warn("Image build not successully completed due to exception" + e.getMessage() );
					 }
				} else {
					LOGGER.info("No Builds In Queue");
				}
				Thread.sleep(2000);
			}
		} catch (Exception e) {
			Helper.printStackTrace(e);
			LOGGER.error(e.getMessage());
		}
	}
   

/*	private String prepareBuildCommand(final String imageFileName) {
		
		//RMAC addresses formation
		StringBuilder bufferRMAC = new StringBuilder();
		for(int cursor=0;cursor<requestvo.getrMacAddress().size();cursor++)
		{
			bufferRMAC.append("RMAC").append(String.valueOf(cursor+1)).append("=").append(requestvo.getrMacAddress().get(cursor));
			bufferRMAC.append(" ");
		}
		LOGGER.info(bufferRMAC.toString().trim());
		//VLAN MAC addresses formation
		StringBuilder bufferVLANMAC = new StringBuilder();
		if(!requestvo.getVlanMacAddress().isEmpty()){
			
		     for(int cursor=0;cursor<requestvo.getVlanMacAddress().size();cursor++)
		     {
			     bufferVLANMAC.append("VLAN").append(String.valueOf(cursor+1)).append("=").append(requestvo.getVlanMacAddress().get(cursor));
			     bufferVLANMAC.append(" ");
		     }
	     }
		//APP MAC address formation
		StringBuilder bufferAPPMAC = new StringBuilder();
		if(!requestvo.getAppMacAddress().isEmpty()){
		     for(int cursor=0;cursor<requestvo.getAppMacAddress().size();cursor++)
		     {
		    	 bufferAPPMAC.append("APPMAC").append(String.valueOf(cursor+1)).append("=").append(requestvo.getAppMacAddress().get(cursor)).toString();//prepare APP MAC address formation fit for command
		    	 bufferAPPMAC.append(" ");
		     }
	     }
	
		//Native command preparation
		
		//windows
		//return new StringJoiner(" ").add("cmd.exe /c echo").add("MAC="+requestvo.getBoardMacAddress())
	    		 .add("MD_CONFIG="+requestvo.getModelConf()).add(bufferRMAC.toString().trim())
	    		 .add(bufferVLANMAC.toString().trim()).add(bufferAPPMAC.toString().trim()).add(">D:/Azure/"+imageFileName+".bin").toString();
		
		//linux
	   return new StringJoiner(" ").add("sudo make -f").add(imageFileName+".bin").add(requestvo.getTarget()).add("MAC="+requestvo.getBoardMacAddress())
	    		 .add("MD_CONFIG="+requestvo.getModelConf()).add(bufferRMAC.toString().trim())
	    		 .add(bufferVLANMAC.toString().trim()).add(bufferAPPMAC.toString().trim()).toString();
	
		return new StringJoiner(" ").add(Constant.BUILD_FILE).add(imageFileName.substring(0, 10)).add("MAC="+requestvo.getBoardMacAddress())
	    		 .add("MD_CONFIG="+requestvo.getModelConf()).add(bufferRMAC.toString().trim())
	    		 .add(bufferVLANMAC.toString().trim()).add(bufferAPPMAC.toString().trim()).add("VERSION="+requestvo.getVersion()).toString();
	   
	}
	
*/	
	
	
}
