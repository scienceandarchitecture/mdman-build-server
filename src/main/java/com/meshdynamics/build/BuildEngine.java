package com.meshdynamics.build;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 */
public class BuildEngine implements ServletContextListener {

	/**
	 * Method contextDestroyed.
	 * @param arg0 ServletContextEvent
	 * @see javax.servlet.ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	/**
	 * Method contextInitialized.
	 * @param arg0 ServletContextEvent
	 * @see javax.servlet.ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		new BuildProcessor().start();
	}

}
