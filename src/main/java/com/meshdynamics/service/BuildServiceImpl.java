/**
 * 
 */
package com.meshdynamics.service;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.meshdynamics.build.BuildProcessor;
import com.meshdynamics.common.Constant;
import com.meshdynamics.helper.MDProperties;
import com.meshdynamics.vo.RequestVo;

/**
 * @author Manik Chandra
 *
 * @version $Revision: 1.0 $
 */
@Service
public class BuildServiceImpl implements BuildService {
	
	/**
	 * Field LOGGER.
	 */
	static final Logger LOGGER = Logger.getLogger(BuildServiceImpl.class);

	/* (non-Javadoc)
	 * @see com.meshdynamics.service.BuildService#generateBuild(java.lang.String, com.meshdynamics.vo.GenerateBuildRequestVo)
	 */
	public int generateBuild(String buildName,RequestVo requestVo)throws Exception {
		
		LOGGER.trace("BuildServiceImpl.generateBuild() started");
		int flg=0;
		String buildPath = MDProperties.get(Constant.BUILD_PATH);
		if(null != buildPath){
			String absoluteBuildPath=buildPath+requestVo.getVersion()+"/";
			File folder = new File(absoluteBuildPath);
			if(!folder.exists())
			    folder.mkdir();
			File[] availableBuilds = folder.listFiles();
			
			if(null != availableBuilds && availableBuilds.length>0){
				for(int buildCount=0;buildCount<availableBuilds.length;buildCount++){
					if(buildName.equalsIgnoreCase(availableBuilds[buildCount].toString())){
						flg=1;
						LOGGER.trace("BuildServiceImpl.generateBuild() end");
						return 1;//if build image is already available
					}
				}if(0==flg){//if image is not available,put request in queue
					BuildProcessor.queueRequest(requestVo);
					LOGGER.trace("BuildServiceImpl.generateBuild() end");
					return 2;
				}
			}else{//if image is not available,put request in queue
				BuildProcessor.queueRequest(requestVo);
				LOGGER.trace("BuildServiceImpl.generateBuild() end");
				return 2;
			}
		}else{
			LOGGER.warn("BuildPath Not Configured, Please configure And Continue !");
			return 3;
		}
		return 0;
	}

	@Override
	public File downloadBuild(String imageName) throws Exception {
		String buildPath = MDProperties.get("buildPath");
        return new File(buildPath+imageName);
       
	}
	
	
}




















