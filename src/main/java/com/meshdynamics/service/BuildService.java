/**
 * 
 */
package com.meshdynamics.service;

import java.io.File;

import com.meshdynamics.vo.RequestVo;

/**
 * @author Manik Chandra
 *
 * @version $Revision: 1.0 $
 */
public interface BuildService {

	/**
	 * Method isBuildStarted.
	 * 
	 * 
	
	
	 * @param buildName String
	 * @param requestVo RequestVo
	
	
	
	 * @return boolean * @throws Exception * @throws Exception * @throws Exception * @throws Exception * @throws Exception
	 */
	int generateBuild(String buildName,RequestVo requestVo) throws Exception;
	File downloadBuild(String imageName) throws Exception;

}
