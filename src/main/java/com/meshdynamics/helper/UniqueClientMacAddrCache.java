package com.meshdynamics.helper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UniqueClientMacAddrCache {
	

	/**
	 * Field clientCache.
	 */
	private static Map<String, String> clientCache = null;

	/**
	 * Constructor for UniqueClientMacAddrCache.
	 */
	private UniqueClientMacAddrCache() {
	}

	/**
	 * Method getRequestedBuildCache.
	 * 
	
	 * @return Map<String,Session> */

	public static Map<String, String> getClientCache(){
		if (null == clientCache) {
			clientCache = new ConcurrentHashMap<>();
		}
		return clientCache;
	}

}
