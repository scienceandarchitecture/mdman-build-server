package com.meshdynamics.helper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.meshdynamics.vo.RequestVo;

/**
 */
public class RunningBuildCache {
	private static Map<String, RequestVo> runningBuildCache = null;

	private RunningBuildCache() {
	}

	/**
	 * Method getRunningBuildCache.
	 * @return Map<String,RequestVo>
	 */
	public static Map<String, RequestVo> getRunningBuildCache() {
		if (null == runningBuildCache) {
			runningBuildCache = new ConcurrentHashMap<>();
		}
		return runningBuildCache;
	}
}
