package com.meshdynamics.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.StringJoiner;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.meshdynamics.common.Constant;
import com.meshdynamics.global.GlobalDirectoryLookup;

public class Helper {

	static final Logger LOGGER = Logger.getLogger(Helper.class);
	
	
	

	private Helper() {
		super();
		
	}



	public static void printStackTrace(Exception e) {

		String isdebugging_Enabled = MDProperties.get("is_debugging_enabled");
		if (null != isdebugging_Enabled && isdebugging_Enabled.equals("true")) {
			e.printStackTrace();
		}
	}



	public static boolean isBuildAdded(String buildFileName) {
		
		boolean flag = false;
		
		FileChannel fileChannel = null;
		FileLock lock = null;
		try {
			
			String bufferString = buildFileName+ "\t\t\t\t\t ="+ new Date().getTime()+"\n";
			
			ByteBuffer buffer = ByteBuffer.wrap(bufferString.getBytes());
             
			
			File buildhistryFile = GlobalDirectoryLookup.getBuildHistroyLogFile();
			
			Path path = Paths.get(buildhistryFile.toURI());
			fileChannel = FileChannel.open(path,StandardOpenOption.WRITE, StandardOpenOption.APPEND);
			if(fileChannel.size()>0){
				fileChannel.position(fileChannel.size() - 1);
			}
			else{
				fileChannel.position(0);
			}


		    lock = fileChannel.lock();
		    
			if (lock.isShared()) {
				LOGGER.warn(buildFileName + " is shared by multiple threads");
			}
			fileChannel.write(buffer);
			
			flag = true;
		} catch (Exception e) 
		{
			LOGGER.error("Logging BuildHisory file was failure!"+e);
			
		}finally
		{
			if(null != lock)// closing File lock
			{
				try {
					lock.close();
				} catch (IOException e) {
					
					LOGGER.error("Locking BuildHisory file was failure!"+e);
				}
			}
			
			if(null != fileChannel)
			{
				try {
					fileChannel.close();// closing the File Channel
				} catch (IOException e) {
					
					LOGGER.error("Logging BuildHisory file was failure!"+e);
				}
			}
		}
		return flag;
	}
	
	public static String constructTargetType(String target)
	
	{
		
		
		StringBuilder targetName = new StringBuilder();
		if("CNS".equalsIgnoreCase(target))
		{
			targetName.append(target.toLowerCase()).append("3xxx_md");
		}else if("IXP".equalsIgnoreCase(target))
		{
			targetName.append(target.toLowerCase()).append("4xxx_md");
		}else if("IMX".equalsIgnoreCase(target)){
			targetName.append(target.toLowerCase()).append("6").append("_md");
		}else{
			LOGGER.warn("Target field was invaid "+target);
		}
	
		return targetName.toString();
	}
	
	public static String constructTargetBuild(String targetName,String ver,String boardMac,int imageType, String configFilename){
		
		String targetType = "";
		
		if(targetName.startsWith("imx")){//if target type IMX eg: imx6_md to imx_md
			targetType = new StringBuilder(targetName).deleteCharAt(3).toString();
			if (null!=configFilename) {
				if(configFilename.startsWith(Constant.CONF_FILE_1K_START)) {
					targetType=targetType+Constant.POSTFIX_1K_BUILDCOMMAND;
				}
				
			}
		}else {
			targetType = targetName;
		}
		
		String buildStr =  new StringJoiner("_").add(targetType.trim()).add(ver.trim()).add(boardMac.replaceAll(":", "_")).toString();
		return  buildStr.concat(MDProperties.get(Constant.IMAGETYPE+imageType));//Image name
		
	}
	
	public static String getRandomUID(){
		return UUID.randomUUID().toString();
	}

	public static void removeBuildLogEntry(final File buildhstryFile , final String entry)
	{
		
		BufferedReader br = null;
		FileWriter fw = null;
		FileReader fr = null;
		try
		{
			
			 fr = new FileReader(buildhstryFile);
			 br=new BufferedReader(fr);
 
			//String buffer to store contents of the file
			StringBuilder sb = new StringBuilder("");
 
			String line;
 
			while((line=br.readLine())!=null)
			{
				//Store each valid line in the string buffer
				if(line.trim().equalsIgnoreCase(entry.trim())){
					continue;
				}
				
					sb.append(line+"\n");
				
			}
			
 
			fw = new FileWriter(buildhstryFile);
			//Write entire string buffer into the file
			fw.write(sb.toString());
			fw.close();
		}
		catch (Exception e)
		{
			Helper.printStackTrace(e);
			LOGGER.error(e.getStackTrace());
		}finally{
			if(br != null){
				try {
					br.close();
				} catch (IOException e) {
					Helper.printStackTrace(e);
				}
			}
			
			if(fw != null){
				try {
					fw.close();
				} catch (IOException e) {
					Helper.printStackTrace(e);
				}
			}
			if(fr != null){
				try {
					fr.close();
				} catch (IOException e) {
					Helper.printStackTrace(e);
				}
			}
		}
	}



	public static boolean isTimeExceedForEntryInCache(Date incDate, Date currDate,long timeToLive) {
		
		long incTime = incDate.getTime();
		long currTime = currDate.getTime();
		
		return (currTime - incTime) > (timeToLive*60*1000) ? true : false;
		
	}
	

	
}
