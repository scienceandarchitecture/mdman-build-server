package com.meshdynamics.helper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.Session;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */
public class WebSocketMappingCache {

	/**
	 * Field webSocketMappingCache.
	 */
	private static Map<String, Session> webSocketMappingCache = null;

	/**
	 * Constructor for webSocketMappingCache.
	 */
	private WebSocketMappingCache() {
	}

	/**
	 * Method getRequestedBuildCache.
	 * 
	
	 * @return Map<String,Session> */

	public static Map<String, Session> getRequestedBuildCache() {
		if (null == webSocketMappingCache) {
			webSocketMappingCache = new ConcurrentHashMap<>();
		}
		return webSocketMappingCache;
	}
}
