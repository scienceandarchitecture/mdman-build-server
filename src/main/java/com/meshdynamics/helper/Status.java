package com.meshdynamics.helper;

import java.util.List;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */

public class Status {
	/**
	 * Field responseCode.
	 */
	private int responseCode;
	/**
	 * Field responseMessage.
	 */
	private String responseMessage;
	public void setErrors(List<BuildError> errors) {
		this.errors = errors;
	}

	/**
	 * Field responseBody.
	 */
	private Object responseBody;
	
	private String uniqueId;
	private String macAddressDigest;
	private String signedKey;

	

	public String getUniqueId() {
		return uniqueId;
	}


	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}


	public String getMacAddressDigest() {
		return macAddressDigest;
	}


	public void setMacAddressDigest(String macAddressDigest) {
		this.macAddressDigest = macAddressDigest;
	}


	public String getSignedKey() {
		return signedKey;
	}


	public void setSignedKey(String signedKey) {
		this.signedKey = signedKey;
	}

	private String result;
	private List<BuildError> errors;
	
	
	

	public List<BuildError> getErrors() {
		
		return errors;
	}
	

	/**
	
	 * @return the result */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * Method getResponseCode.
	
	 * @return int */
	public int getResponseCode() {
		return responseCode;
	}

	/**
	 * Method setResponseCode.
	 * 
	 * @param responseCode
	 *            int
	 */
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * Method getResponseMessage.
	 * 
	 * 
	
	 * @return String */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * Method setResponseMessage.
	 * 
	 * @param responseMessage
	 *            String
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * Method getResponseBody.
	 * 
	 * 
	
	 * @return Object */
	public Object getResponseBody() {
		return responseBody;
	}

	/**
	 * Method setResponseBody.
	 * 
	 * @param responseBody
	 *            Object
	 */
	public void setResponseBody(Object responseBody) {
		this.responseBody = responseBody;
	}

	/**
	 * Method toString.
	 * 
	 * 
	
	 * @return String */
	@Override
	public String toString() {
		return "Status [responseCode=" + responseCode + ", responseMessage="
				+ responseMessage + ", responseBody=" + responseBody + "]";
	}
}
