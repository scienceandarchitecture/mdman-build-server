package com.meshdynamics.helper;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */
public class HttpStatus {
	
	/**
	 * Field OK.
	 */
	public static int OK 					= 	200;
	/**
	 * Field CREATED.
	 */
	public static int CREATED 				= 	201;
	/**
	 * Field EXISTs.
	 */
	public static int EXISTS 				= 	202;
	/**
	/**
	 * Field SUCCESS.
	 */
	public static int SUCCESS				=	1;
	/**
	 * Field FAILED.
	 */
	public static int FAILED				=	0;
	/**
	 * Field NO_RECORDS.
	 */
	public static int NO_RECORDS			=	2;
	/**
	 * Field EMPTY_BODY_REQUEST.
	 */
	public static int EMPTY_BODY_REQUEST	=	3;
	/**
	 * Field SYSTEM_EXCEPTION.
	 */
	public static int SYSTEM_EXCEPTION		=	4;
	/**
	 * Field PARTIAL_CONTENT.
	 */
	public static int PARTIAL_CONTENT		=	206;
	/**
	 * Field UNAUTHORISED.
	 */
	public static int UNAUTHORISED			=	401;
	/**
	 * Field UNSUPPORTED_MEDIA.
	 */
	public static int UNSUPPORTED_MEDIA		=	415;
	/**
	 * Field NOT_IMPLEMENTED.
	 */
	public static int NOT_IMPLEMENTED		=	501;
	/**
	 * Field LOGIN_TIMEOUT.
	 */
	public static int LOGIN_TIMEOUT			=  	440;
	/**
	 * Field BAD_REQUEST.
	 */
	public static int BAD_REQUEST			=	400;
	
	/**
	 * Field RESOURCE_NOT_FOUND.
	 */
	public static int RESOURCE_NOT_FOUND			=	404;
	
	
	/**
	 * Field GET.
	 */
	public static int GET					=	0;
	/**
	 * Field POST.
	 */
	public static int POST					=	1;
	/**
	 * Field PUT.
	 */
	public static int PUT					=	2;
	/**
	 * Field DELETE.
	 */
	public static int DELETE				=	3;
	
	
	
}
