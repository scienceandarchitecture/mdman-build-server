package com.meshdynamics.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.meshdynamics.common.Constant;
import com.meshdynamics.global.GlobalDirectoryLookup;

public class BuildCleaner extends TimerTask {
	static final Logger LOGGER = Logger.getLogger(BuildCleaner.class);
	private static final long CLEAN_PERIOD = 10000L;
	private static Timer timer = null;
	

	BuildCleaner() {
		
		if (null != timer) {
			timer.cancel();
			timer = null;
		}
		timer = new Timer();
		timer.schedule(this, 0, CLEAN_PERIOD);
	}

	@Override
	public void run() {
		
		BufferedReader br = null;

		try {
			List<String> list = new ArrayList<>();
            File file = GlobalDirectoryLookup.getBuildHistroyLogFile();
            if(!file.exists()){
            	LOGGER.warn("BuildHistory.log file not generated yet:"+file.toPath());
            	return;
            }
			 br = Files.newBufferedReader(Paths.get(file.toURI()));
			list = br.lines().collect(Collectors.toList());
				
			

			if (null != list && list.size() > 0) {
				for (int i = 1; i < list.size(); i++) {
					String buildFileName = list.get(i).split("=")[0];
					String generatedTime = list.get(i).split("=")[1];
					long currentTime = new Date().getTime();
					if (currentTime - Long.parseLong(generatedTime) > (Long.parseLong(MDProperties.get(Constant.BUILD_CLEAN_TIME))*60000)) {
						try{
							UniqueClientMacAddrCache.getClientCache().remove(buildFileName.split("/").clone()[1]);
						}catch( Exception e){
							LOGGER.warn(e.getLocalizedMessage());
						}
						Path absoluteFilePath = GlobalDirectoryLookup.getRootDirectory().toPath().resolve(buildFileName.trim());
						Helper.removeBuildLogEntry(file, list.get(i));
						if (Files.deleteIfExists(Paths.get(absoluteFilePath.toUri()))) {
							LOGGER.info(buildFileName + "Cleaned Successfully");
							
							
						}
					} else {
						LOGGER.warn(buildFileName+ "Updated File can't be deleted!");
					}
				}
			} else {
				LOGGER.info("Images Not Generated !");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}finally{
			if(null != br){
				try {
					br.close();
				} catch (IOException e) {
					LOGGER.error(e.getMessage());
				}
			}
		}
	}
}
