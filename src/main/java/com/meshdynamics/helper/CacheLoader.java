package com.meshdynamics.helper;

import java.util.Base64;
import java.util.StringJoiner;

import javax.crypto.SecretKey;

import org.apache.log4j.Logger;

import com.meshdynamics.security.SecurityGateWay;
import com.meshdynamics.vo.RequestVo;

public class CacheLoader {
	
	/**
	 * Field LOGGER.
	 */
	static final Logger LOGGER = Logger.getLogger(CacheLoader.class);

	private CacheLoader() {
		super();
		
	}
	
	public static void loadCache(RequestVo requestVo,String uid){
		LOGGER.info("CacheLoader.loadCache() started");
       // create new key
        SecretKey key = SecurityGateWay.getSecretKey();
        // get base64 encoded version of the key
        String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());
        UniqueClientMacAddrCache.getClientCache().put(uid,new StringJoiner(",").add(encodedKey).add(requestVo.getClientMacAddress()).toString());
	    LOGGER.info("CacheLoader.loadCache() end");
	}

}
