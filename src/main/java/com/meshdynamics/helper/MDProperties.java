package com.meshdynamics.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */
public class MDProperties {

	/**
	 * Field in.
	 */
	static InputStream in = null;

	/**
	 * Method get.
	 * 
	 * @param attribute
	 *            String
	 * 
	 * @return String
	 */
	public static String get(String attribute) {
		String result = "";
		try {
			in = MDProperties.class.getResourceAsStream("/md.properties");
			Properties properties = new Properties();
			properties.load(in);
			result = properties.getProperty(attribute);
		} catch (IOException e) {
			Helper.printStackTrace(e);
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					Helper.printStackTrace(e);
				}
			}
		}
		return result;
	}
}
