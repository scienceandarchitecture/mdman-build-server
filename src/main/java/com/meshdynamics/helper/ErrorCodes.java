package com.meshdynamics.helper;

public enum ErrorCodes {
	
	VALIDATION_FAILED("1001","Request validation failed"),
	API_KEY_INVALID("1004","API Key is invalid"),
	WEB_SOCKET_ERROR("1002","Web Socket Error"),
	BUILD_SERVER_ERROR("1003","Build Server Error"),
	SECURITY_ERROR("1005","Security Error");
	
	
	
	public String getCode() {
		return code;
	}
	public String getValue() {
		return value;
	}
	private String code;
	private String value;
	private ErrorCodes(String code, String value) {
		this.code = code;
		this.value = value;
	}

}
