package com.meshdynamics.helper;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import javax.websocket.Session;

import org.apache.log4j.Logger;

import com.meshdynamics.common.Constant;
import com.meshdynamics.vo.GenerateBuildRequestVo;
import com.meshdynamics.vo.RequestVo;

/**
 */
public class UpdateBuildStatus extends TimerTask {
	
	static final Logger LOGGER = Logger.getLogger(UpdateBuildStatus.class);
	private static final long UPDATE_PERIOD = 30000L;
	private static Timer timer = null;
	Map<String, GenerateBuildRequestVo> map = null;

	UpdateBuildStatus() {
		if (null != timer) {
			timer.cancel();
			timer = null;
		}
		timer = new Timer();
		timer.schedule(this, 0, UPDATE_PERIOD);
	}

	/**
	 * Method run.
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		Map<String, RequestVo> waitingBuilds = RunningBuildCache.getRunningBuildCache();
		try{
			if (null != waitingBuilds && waitingBuilds.size() > 0) {
				Iterator<?> entries = waitingBuilds.entrySet().iterator();
				String buildPath = MDProperties.get(Constant.BUILD_PATH);
				
				while (entries.hasNext()) {
					Entry<?, ?> thisEntry = (Entry<?, ?>) entries.next();
					Object value = thisEntry.getValue();
					Object key	 = thisEntry.getKey();
					RequestVo requestVo = (RequestVo) value;
					
					//if generated build live more than a hour should be removed from running build cache
					final Date incDate = requestVo.getIncidentDate();
					final Date currDate = new Date();
					final long timeToLiveInCache = Long.parseLong(MDProperties.get(Constant.BUILD_LIVE_IN_CACHE));
					boolean timeFlag = Helper.isTimeExceedForEntryInCache(incDate , currDate , timeToLiveInCache);//if true when entry time exceeds the given time in cache
					if(timeFlag)
					{
						waitingBuilds.remove(key);
						LOGGER.info("Entry Removed from Build Cache for Key - "+key);
					}
					
					final String buildName =requestVo.getBuildName();
					String absolutePath = buildPath + requestVo.getVersion() + "/";
					File folder = new File(absolutePath);
					File[] availableBuilds = folder.listFiles();
					if (null != availableBuilds && availableBuilds.length > 0) {
						for (int buildCount = 0; buildCount < availableBuilds.length; buildCount++) {
							if (buildName.equalsIgnoreCase(availableBuilds[buildCount].getName())) {
								Session session = WebSocketMappingCache.getRequestedBuildCache().get(requestVo.getClientMacAddress());
								if(null!=session){// For testing only
									session.getBasicRemote().sendText(requestVo.getVersion() + "/"+requestVo.getBuildName());
									//removing the macAddress if builds completed
									RunningBuildCache.getRunningBuildCache().remove(requestVo.getMacAddress());
									
									//Add Generated Build to File with currentTime stamp
									
									LOGGER.info("Build Completed For -"+requestVo.getMacAddress());
									break;
								}
							}else{
								LOGGER.info("Build is in-Progress For - "+requestVo.getMacAddress());
							}
						}
					}
				}
			}else{
				LOGGER.info("No Builds In Queue");
			}
		}catch(Exception e){
			Helper.printStackTrace(e);
		}
	}
}