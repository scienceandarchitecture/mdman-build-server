package com.meshdynamics.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

public class DownloadBuildRequestVo {
	
	
	
	@NotEmpty
	private String uniqueId;
	@NotEmpty
	private String imageName;
	@NotEmpty
	@Pattern(regexp="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
	private String clientMacAddress;

	private DownloadBuildRequestVo() {
		super();
		
	}
	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}


	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getClientMacAddress() {
		return clientMacAddress;
	}

	public void setClientMacAddress(String clientMacAddress) {
		this.clientMacAddress = clientMacAddress;
	}


	

}
