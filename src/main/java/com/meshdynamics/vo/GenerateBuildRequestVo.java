package com.meshdynamics.vo;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */

public class GenerateBuildRequestVo {

	@NotEmpty
	// @Pattern(regexp="^[a-zA-Z0-9]+$")
	private String target;

	@NotEmpty
	// @Pattern(regexp="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
	private String boardMacAddress;

	@NotEmpty
	private String modelConf;

	@NotEmpty
	// @Pattern(regexp="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
	private List<String> rMacAddress;

	// @Pattern(regexp="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
	private List<String> vlanMacAddress;

	// @Pattern(regexp="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
	private List<String> appMacAddress;

	@NotEmpty
	/// @Pattern(regexp="^[0-9]+\\.[0-9]+\\.[0-9]+$")
	private String version;

	@NotEmpty
	// @Pattern(regexp="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
	private String clientMacAddress;
    
	private int imageType;

	public GenerateBuildRequestVo() {
		super();

	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getBoardMacAddress() {
		return boardMacAddress;
	}

	public void setBoardMacAddress(String boardMacAddress) {
		this.boardMacAddress = boardMacAddress;
	}

	public String getModelConf() {
		return modelConf;
	}

	public void setModelConf(String modelConf) {
		this.modelConf = modelConf;
	}

	public List<String> getrMacAddress() {
		return rMacAddress;
	}

	public void setrMacAddress(List<String> rMacAddress) {
		this.rMacAddress = rMacAddress;
	}

	public List<String> getVlanMacAddress() {
		return vlanMacAddress;
	}

	public void setVlanMacAddress(List<String> vlanMacAddress) {
		this.vlanMacAddress = vlanMacAddress;
	}

	public List<String> getAppMacAddress() {
		return appMacAddress;
	}

	public void setAppMacAddress(List<String> appMacAddress) {
		this.appMacAddress = appMacAddress;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getClientMacAddress() {
		return clientMacAddress;
	}

	public void setClientMacAddress(String clientMacAddress) {
		this.clientMacAddress = clientMacAddress;
	}

	public int getImageType() {
		return imageType;
	}

	public void setImageType(int imageType) {
		this.imageType = imageType;
	}

}
