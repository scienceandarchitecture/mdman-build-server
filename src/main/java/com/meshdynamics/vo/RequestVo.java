package com.meshdynamics.vo;

import java.util.Date;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */
public class RequestVo {
	/**
	 * Field command.
	 */
	private String command;
	/**
	 * Field version.
	 */
	private String version;
	/**
	 * Field macAddress.
	 */
	private String macAddress;
	/**
	 * Field buildName.
	 */
	private String buildName;
	/**
	 * Field clientMacAddress.
	 */
	private String clientMacAddress;
	
	/**
	 * Field incidentDate.
	 */
	private Date incidentDate;
	
	/**
	 * Field targetType
	 * @return
	 */
	private String targetType;


	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public Date getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	/**
	
	 * @return the command */
	public String getCommand() {
		return command;
	}

	/**
	 * @param command
	 *            the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}

	/**
	
	 * @return the version */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	
	 * @return the macAddress */
	public String getMacAddress() {
		return macAddress;
	}

	/**
	 * @param macAddress
	 *            the macAddress to set
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	/**
	
	 * @return the buildName */
	public String getBuildName() {
		return buildName;
	}

	/**
	 * @param buildName
	 *            the buildName to set
	 */
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}

	public String getClientMacAddress() {
		return clientMacAddress;
	}

	public void setClientMacAddress(String clientMacAddress) {
		this.clientMacAddress = clientMacAddress;
	}

}
