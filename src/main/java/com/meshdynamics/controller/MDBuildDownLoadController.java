package com.meshdynamics.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.exceptions.RequestPreConditionsFailedException;
import com.meshdynamics.helper.Helper;
import com.meshdynamics.helper.HttpStatus;
import com.meshdynamics.helper.Status;
import com.meshdynamics.helper.UniqueClientMacAddrCache;
import com.meshdynamics.service.BuildServiceImpl;
import com.meshdynamics.vo.DownloadBuildRequestVo;
@Controller
@RequestMapping("/build")
public class MDBuildDownLoadController {
     
	
	/**
	 * Field LOGGER.
	 */
	static final Logger LOGGER = Logger.getLogger(MDBuildController.class);
	/**
	 * Field buildServiceImpl.
	 */
	@Autowired
	private BuildServiceImpl buildServiceImpl;
	
	
	
	/**
	 * Method downloadBuild.
	 * @param response HttpServletResponse
	 * @param authenticationKey String
	 * @param requestbody String
	 */

	@CrossOrigin
	@RequestMapping(value = "/download", method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Status> downloadBuild(HttpServletRequest request , HttpServletResponse response,@RequestHeader(value = "Authentication") String authenticationKey,@Valid @RequestBody DownloadBuildRequestVo downloadBuildRequestVo,BindingResult bindingResult) {

		LOGGER.trace("MDBuildDownLoadController.downloadBuild() started");
		Status status  = new Status();
		InputStream in = null;
		try {
			if(bindingResult.hasErrors()){//if true , Request precondition are not meet
		         throw new RequestPreConditionsFailedException("Invalid Request", bindingResult);
          }
	
			//delegating the service to process the request if request is valid
			final String uniqueId = downloadBuildRequestVo.getUniqueId();
			final String secKeyAndAddr = UniqueClientMacAddrCache.getClientCache().get(uniqueId);
			
			final boolean isSecreyKeyMatched = secKeyAndAddr.split(",")[0].equals(authenticationKey);
			final boolean isClientMacMatched = secKeyAndAddr.split(",")[1].equals(downloadBuildRequestVo.getClientMacAddress());
			if ( isSecreyKeyMatched && isClientMacMatched ) {
				final String buildName = downloadBuildRequestVo.getImageName();
				final File file = buildServiceImpl.downloadBuild(buildName);
				//if image file not exists
				if(!file.exists()){
					LOGGER.trace("Requested Build was not found in traget direcroty-"+buildName);
					status.setResponseCode(HttpStatus.NO_RECORDS);
					status.setResponseMessage("Requested Build was not found-"+buildName);
					status.setResponseBody("Requested Build was not found in traget direcroty-"+buildName);
					status.setResult(Constant.FAILED);
					
				}
				
			    
				response.setContentType("image/bin");
				//response.setHeader("Access-Control-Allow-Origin","*");
				//response.setHeader("Access-Control-Allow-Credentials",  "true");
				
				response.setHeader("Content-Disposition","attachment; filename=" + file.getName());
				response.setHeader("Content-Length",String.valueOf(file.length()));
				in = new FileInputStream(file);
				FileCopyUtils.copy(in, response.getOutputStream());
				
				LOGGER.trace("Requested Build was found in traget direcroty-"+buildName);
				status.setResponseCode(HttpStatus.OK);
				status.setResponseMessage("Requested Build available-"+buildName);
				StringBuffer buffer = request.getRequestURL();
				buffer.append("/").append(buildName);
				LOGGER.info("Prepared Download URL-"+buffer.toString());
				status.setResponseBody(buffer.toString());
				status.setResult(Constant.SUCCESS);
		
				
			} else {
				LOGGER.info("UnAuthorized access");
				status.setResponseCode(HttpStatus.UNAUTHORISED);
				status.setResponseMessage("UnAuthorized user trying to access the resource");
				status.setResponseBody("Authentication Failed!");
				status.setResult(Constant.FAILED);
			}
		  

		} catch (Exception e) {
			
			status.setResponseCode(HttpStatus.SYSTEM_EXCEPTION);
			status.setResponseMessage("Server Error");
			status.setResponseBody(e.getLocalizedMessage());
			status.setResult(Constant.FAILED);
			Helper.printStackTrace(e);
		}finally{
			if(in !=null){
				try {
					in.close();
				} catch (IOException e) {
					Helper.printStackTrace(e);
				}
			}
		}
		
		LOGGER.trace("MDBuildDownLoadController.downloadBuild() ends");
		return new ResponseEntity<Status>(status, org.springframework.http.HttpStatus.OK);
	}
	
	
}
