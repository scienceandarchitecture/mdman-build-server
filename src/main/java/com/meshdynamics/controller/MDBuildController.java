package com.meshdynamics.controller;



import java.io.File;
import java.util.Date;
import java.util.StringJoiner;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.exceptions.RequestPreConditionsFailedException;
import com.meshdynamics.global.GlobalDirectoryLookup;
import com.meshdynamics.helper.CacheLoader;
import com.meshdynamics.helper.Helper;
import com.meshdynamics.helper.HttpStatus;
import com.meshdynamics.helper.MDProperties;
import com.meshdynamics.helper.RunningBuildCache;
import com.meshdynamics.helper.Status;
import com.meshdynamics.helper.UniqueClientMacAddrCache;
import com.meshdynamics.helper.WebSocketMappingCache;
import com.meshdynamics.security.SecurityGateWay;
import com.meshdynamics.service.BuildServiceImpl;
import com.meshdynamics.vo.GenerateBuildRequestVo;
import com.meshdynamics.vo.RequestVo;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */

@Controller
@RequestMapping("/build")
public class MDBuildController {

	/**
	 * Field LOGGER.
	 */
	static final Logger LOGGER = Logger.getLogger(MDBuildController.class);
	/**
	 * Field buildServiceImpl.
	 */
	@Autowired
	private BuildServiceImpl buildServiceImpl;
	
	
	/**
	 * Method generateBuild.
	 * @param request request
	 * @param bindingResult {@link BindingResult}
	 * @return Status */

	@RequestMapping(value = "/generate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Status> generateBuild(@Valid @RequestBody GenerateBuildRequestVo request,BindingResult bindingResult,@RequestHeader(value = "Authentication") String authenticationKey) {

		LOGGER.trace("MDBuildController.generateBuild() started");
		
		int flg=0;
		Status status = new Status();
		try {
			  if(bindingResult.hasErrors()){//if true , Request precondition are not meet
			      throw new RequestPreConditionsFailedException("Invalid Request", bindingResult);
	          }
		     //delegating the service to process the request if request is valid
			  
			  String targetType = Helper.constructTargetType(request.getTarget());
			  
			  
			  //prepare image file name on type of target
			  final String buildName = Helper.constructTargetBuild(targetType, request.getVersion(), request.getBoardMacAddress(),request.getImageType(),request.getModelConf()	);
			  
			  final String buildCommand= prepareBuildCommand(targetType,request);
		      LOGGER.info("Prepared command - "+buildCommand);
			  RequestVo requestVo=new RequestVo();
			  requestVo.setBuildName(buildName);
			  requestVo.setCommand(buildCommand);
			  requestVo.setVersion(request.getVersion());
			  requestVo.setMacAddress(request.getBoardMacAddress()); 
			  requestVo.setClientMacAddress(request.getClientMacAddress());
			  requestVo.setIncidentDate(new Date());
			  
			  if (!(request.getModelConf().isEmpty())) {
					if(request.getModelConf().startsWith(Constant.CONF_FILE_1K_START)){
						targetType = targetType+"_1k";
					}
			  }
			 requestVo.setTargetType(targetType);
			
			  
			  
			//Lookup cache if client mac address and requested client mac address matches

			final String targetClient = request.getClientMacAddress();
			final String uid=SecurityGateWay.bytesToHex(buildName.getBytes());


			String signedKey=null; 
			//String keyAndmac = UniqueClientMacAddrCache.getClientCache().get(uid);
			if (authenticationKey.equalsIgnoreCase(SecurityGateWay.bytesToHex(targetClient.getBytes()))) {

				if (WebSocketMappingCache.getRequestedBuildCache().containsKey(targetClient)) {
				   
					LOGGER.info("Web socket session is live for client - "+targetClient);
					
				  //check if build already exists in target directory	
					File buildCfgFile = GlobalDirectoryLookup.getRootDirectory();
					boolean isBuildAvailableinCache = UniqueClientMacAddrCache.getClientCache().containsKey(uid)?true:false;
					
					if(buildCfgFile != null && buildCfgFile.exists()){// check configuration dir exists or not
						File buildFile = buildCfgFile.toPath().resolve(request.getVersion()).resolve(buildName).toFile();
						LOGGER.info(" Getting build file Path - "+buildFile.toPath());
					
						//check if build directory configured or not
						if(buildFile != null && buildFile.exists()){
							
							//verify the client cache if build is available or not
							if(isBuildAvailableinCache){
								signedKey = UniqueClientMacAddrCache.getClientCache().get(uid).split(",")[0];
						     }else{
						    	 CacheLoader.loadCache(requestVo, uid);
						    	 signedKey = UniqueClientMacAddrCache.getClientCache().get(uid).split(",")[0];
						     }
						
						    flg=1;
						}else{
								//check if build is already exist in running cache for this request else proceed to start build
								RequestVo vo = RunningBuildCache.getRunningBuildCache().get(request.getBoardMacAddress());
								if(vo != null && vo.getBuildName().equalsIgnoreCase(buildName)){
									flg = 2;
								}else{
									
									if(isBuildAvailableinCache){
										UniqueClientMacAddrCache.getClientCache().remove(uid);
									}
									
									flg = buildServiceImpl.generateBuild(buildName,requestVo);
									CacheLoader.loadCache(requestVo, uid);
									signedKey = UniqueClientMacAddrCache.getClientCache().get(uid).split(",")[0];
									
								}
								
							}
			
					}else{
						flg=3;
					}
					
		
					

				} else {// if target client is not found in web socket cache
					flg = 4;
				}
			} else {
				flg = 4;
			}

			LOGGER.info("Flg value - "+flg);
			if(1==flg){
				LOGGER.info("Image already exists for "+request.getBoardMacAddress());
				status.setResponseCode(HttpStatus.EXISTS);
				status.setResponseMessage("Image already exists, Please download !");
				status.setResponseBody(request.getVersion()+"/"+buildName);
				status.setResult(Constant.SUCCESS);
				status.setUniqueId(uid);
				status.setMacAddressDigest(SecurityGateWay.encryptData(request.getBoardMacAddress(),signedKey));
				status.setSignedKey(signedKey);
			 }else if(2==flg){
				LOGGER.info("Build Started for "+request.getBoardMacAddress());
				status.setResponseCode(HttpStatus.CREATED);
				status.setResponseMessage("Build Started for "+request.getBoardMacAddress()+" Please Wait !");
				status.setResponseBody(request.getVersion()+"/"+buildName);
				status.setResult(Constant.IN_PROGRESS);
				status.setUniqueId(uid);
				status.setMacAddressDigest(SecurityGateWay.encryptData(request.getBoardMacAddress(),signedKey));
				status.setSignedKey(signedKey);
			}else if(3==flg){
				LOGGER.warn("Build Not Started for "+request.getBoardMacAddress());
				status.setResponseCode(HttpStatus.FAILED);
				status.setResponseMessage("BuildPath Not configured, Please configure and continue !");
				status.setResponseBody("");
				status.setResult(Constant.FAILED);
				//status.setUniqueId("0");
				//status.setMacAddressDigest(SecurityGateWay.encryptData(request.getBoardMacAddress(),signedKey));
				//status.setSignedKey("0");
			}else if(4==flg){
				LOGGER.warn("Build Not Started for "+request.getBoardMacAddress());
				status.setResponseCode(HttpStatus.FAILED);
				status.setResponseMessage("Websocket not available unautherised request !");
				status.setResponseBody("");
				status.setResult(Constant.FAILED);
				//status.setUniqueId("0");
				//status.setMacAddressDigest(SecurityGateWay.encryptData(request.getBoardMacAddress(),signedKey));
				//status.setSignedKey(SecurityGateWay.getSecretKeyInHex());
			}
			else{
				LOGGER.warn("Build Not Generated for "+request.getBoardMacAddress());
				status.setResponseCode(HttpStatus.FAILED);
				status.setResponseMessage("Build Not Generated");
				status.setResponseBody("");
				status.setResult(Constant.FAILED);
				//status.setUniqueId("0");
				//status.setMacAddressDigest(SecurityGateWay.encryptData(request.getBoardMacAddress(),signedKey));
				//status.setSignedKey("0");
			  }
		

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			status.setResponseCode(HttpStatus.NOT_IMPLEMENTED);
			status.setResponseMessage("Build Not Started");
			status.setResponseBody(e.getLocalizedMessage());
			status.setResult(Constant.FAILED);
		}
		  
	 
		  
		
		LOGGER.trace("MDBuildController.generateBuild() end");
		
		return new ResponseEntity<Status>(status, org.springframework.http.HttpStatus.OK);

	}
	
	private String prepareBuildCommand(final String imageFileName, GenerateBuildRequestVo generateBuildRequestVo) {
		
		//RMAC addresses formation
		StringBuilder bufferRMAC = new StringBuilder();
		for(int cursor=0;cursor<generateBuildRequestVo.getrMacAddress().size();cursor++)
		{
			bufferRMAC.append("RMAC").append(String.valueOf(cursor+1)).append("=").append(generateBuildRequestVo.getrMacAddress().get(cursor));
			bufferRMAC.append(" ");
		}
		LOGGER.info(bufferRMAC.toString().trim());
		//VLAN MAC addresses formation
		StringBuilder bufferVLANMAC = new StringBuilder();
		if(!generateBuildRequestVo.getVlanMacAddress().isEmpty()){
			
		     for(int cursor=0;cursor<generateBuildRequestVo.getVlanMacAddress().size();cursor++)
		     {
			     bufferVLANMAC.append("VLAN").append(String.valueOf(cursor+1)).append("=").append(generateBuildRequestVo.getVlanMacAddress().get(cursor));
			     bufferVLANMAC.append(" ");
		     }
	     }
		//APP MAC address formation
		StringBuilder bufferAPPMAC = new StringBuilder();
		if(!generateBuildRequestVo.getAppMacAddress().isEmpty()){
		     for(int cursor=0;cursor<generateBuildRequestVo.getAppMacAddress().size();cursor++)
		     {
		    	 bufferAPPMAC.append("APPMAC").append(String.valueOf(cursor+1)).append("=").append(generateBuildRequestVo.getAppMacAddress().get(cursor)).toString();//prepare APP MAC address formation fit for command
		    	 bufferAPPMAC.append(" ");
		     }
	     }
		StringBuilder postfixImageType= new StringBuilder();
		StringBuilder additionalParameter=new StringBuilder();
		if (!(generateBuildRequestVo.getModelConf().isEmpty())) {
			if(generateBuildRequestVo.getModelConf().startsWith(Constant.CONF_FILE_1K_START)) {
				postfixImageType.append(Constant.POSTFIX_1K_BUILDCOMMAND);
				additionalParameter.append(Constant.ADDITIONAL_PARAM_1K_BUILDCOMMAND);
			}
			
		}
		
		return new StringJoiner(" ").add(Constant.BUILD_FILE).add(imageFileName+postfixImageType.toString()+MDProperties.get(Constant.COMMANDIMAGETYPE+generateBuildRequestVo.getImageType())).add("MAC="+generateBuildRequestVo.getBoardMacAddress())
	    		 .add("MD_CONFIG="+generateBuildRequestVo.getModelConf()).add(bufferRMAC.toString().trim())
	    		 .add(bufferVLANMAC.toString().trim()).add(bufferAPPMAC.toString().trim()).add(additionalParameter).add("VERSION="+generateBuildRequestVo.getVersion()).toString();
	   
	}

}
