package com.meshdynamics.common;

import com.meshdynamics.helper.MDProperties;

/**
 * @author Manik Chandra
 * @version $Revision: 1.0 $
 */
public interface Constant {
	/**
	 * Field FAILED. (value is ""Failed"")
	 */
	public static final String FAILED = "Failed";
	/**
	 * Field SUCCESS. (value is ""Success"")
	 */
	public static final String SUCCESS = "Success";

	/**
	 * Field BUILD_PATH. (value is ""buildPath"")
	 */
	public static final String BUILD_PATH = "buildPath";
	public static final String BUILD_COMMAND_PATH = "buildCommandPath.";
	
	public static final String BUILD_HISTORY_LOG = "buildhistory_File";

	public static final String IN_PROGRESS = "Inprogress";

	public static final long UPDATE_BUILD_STATUS_PERIOD = Long.parseLong(MDProperties.get("update_build_status_period"));
	
	public static final String IMAGETYPE="imageType.";
	public static final String BUILD_FILE = "sudo make";
	
	public static final String BUILD_CLEAN_TIME="buildCleanTime";
	
	public static final String BUILD_LIVE_IN_CACHE = "timeToLiveInCache";
	
	public static final String COMMANDIMAGETYPE="commandImageType.";
	
	public static final String BUILD_COMMAND_DIR = "buildCommandDir";
	
	public static final String CONF_FILE_1K_START = "MD1";
	
	public static final String POSTFIX_1K_BUILDCOMMAND = "1k";
	
	public static final String ADDITIONAL_PARAM_1K_BUILDCOMMAND = "BOARD=nor FLASHSIZE=16";
	
}
